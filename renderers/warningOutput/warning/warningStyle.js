const warningStyle = {
	warningStyle: { 
		width: '100%',
		margin: '5px 0',
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	contentStyle: {
		minWidth: '240px',
		margin: '5px 0',
		padding: '0 8px',
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexWrap: 'wrap',
		border: '1px solid #eee',
		backgroundColor: 'beige',
		borderRadius: '5px'
	},
	iconStyle: {
		color: 'goldenrod',
		marginRight: '5px',
		width: '20px',
		height: '20px'
	},
	titleStyle: {
		fontWeight: 'bold',
		marginRight: '6px'
	}
};

export default warningStyle;
