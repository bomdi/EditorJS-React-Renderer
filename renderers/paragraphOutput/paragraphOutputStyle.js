import { MARGIN } from '../../style/template';

const paragraphOutputStyle = { 
	margin: MARGIN, 
	textAlign: 'left',
};

export default paragraphOutputStyle;
